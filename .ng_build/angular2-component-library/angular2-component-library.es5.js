import { Component, EventEmitter, Input, NgModule, Output } from '@angular/core';
import { CommonModule } from '@angular/common';
var CounterComponent = (function () {
    function CounterComponent() {
        this.countChange = new EventEmitter();
        this.min = 0;
        this.max = 100;
    }
    /**
     * @return {?}
     */
    CounterComponent.prototype.ngOnInit = function () {
        this.count = this.count || this.min;
    };
    /**
     * @return {?}
     */
    CounterComponent.prototype.increase = function () {
        if (this.isMax()) {
            return;
        }
        this.countChange.emit(this.count + 1);
    };
    /**
     * @return {?}
     */
    CounterComponent.prototype.isMax = function () {
        return this.count >= this.max;
    };
    /**
     * @return {?}
     */
    CounterComponent.prototype.decrease = function () {
        if (this.isMin()) {
            return;
        }
        this.countChange.emit(this.count - 1);
    };
    /**
     * @return {?}
     */
    CounterComponent.prototype.isMin = function () {
        return this.count <= this.min;
    };
    return CounterComponent;
}());
CounterComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-counter',
                template: "\n    <div class=\"input-group\">\n      <span class=\"input-group-btn\">\n        <button class=\"btn btn-info\" type=\"button\" (click)=\"decrease()\" [disabled]=\"isMin()\">-</button>\n      </span>\n      <input type=\"text\" class=\"form-control\" readonly [value]=\"count\">\n      <span class=\"input-group-btn\">\n        <button class=\"btn btn-primary\" type=\"button\" (click)=\"increase()\" [disabled]=\"isMax()\">+</button>\n      </span>\n    </div>\n  ",
                styles: ["\n\n  "]
            },] },
];
/**
 * @nocollapse
 */
CounterComponent.ctorParameters = function () { return []; };
CounterComponent.propDecorators = {
    'count': [{ type: Input },],
    'countChange': [{ type: Output },],
    'min': [{ type: Input },],
    'max': [{ type: Input },],
};
var CounterModule = (function () {
    function CounterModule() {
    }
    return CounterModule;
}());
CounterModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule
                ],
                declarations: [CounterComponent],
                exports: [CounterComponent]
            },] },
];
/**
 * @nocollapse
 */
CounterModule.ctorParameters = function () { return []; };
/**
 * Generated bundle index. Do not edit.
 */
export { CounterModule, CounterComponent as ɵa };
//# sourceMappingURL=angular2-component-library.es5.js.map
