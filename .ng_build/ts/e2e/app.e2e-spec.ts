import { Angular2ComponentLibraryPage } from './app.po';

describe('angular2-component-library App', () => {
  let page: Angular2ComponentLibraryPage;

  beforeEach(() => {
    page = new Angular2ComponentLibraryPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
