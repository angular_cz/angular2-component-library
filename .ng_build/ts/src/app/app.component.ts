import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <div class="container">
      <h1>Own counter component</h1>
      <div class="row">
        <div class="col-sm-3">
          <app-counter [(count)]="count" [max]="5"></app-counter>
        </div>
      </div>
    </div>
  `,
  styles: [`

  `]
})
export class AppComponent {
  count;
}
