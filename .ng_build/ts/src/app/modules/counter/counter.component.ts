import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-counter',
  template: `
    <div class="input-group">
      <span class="input-group-btn">
        <button class="btn btn-info" type="button" (click)="decrease()" [disabled]="isMin()">-</button>
      </span>
      <input type="text" class="form-control" readonly [value]="count">
      <span class="input-group-btn">
        <button class="btn btn-primary" type="button" (click)="increase()" [disabled]="isMax()">+</button>
      </span>
    </div>
  `,
  styles: [`

  `]
})
export class CounterComponent implements OnInit {

  @Input() count;
  @Output() countChange = new EventEmitter<number>();
  @Input() min = 0;
  @Input() max = 100;

  constructor() {
  }

  ngOnInit() {
    this.count = this.count || this.min;
  }

  increase() {
    if (this.isMax()) {
      return;
    }

    this.countChange.emit(this.count + 1);
  }

  isMax() {
    return this.count >= this.max;
  }

  decrease() {
    if (this.isMin()) {
      return;
    }

    this.countChange.emit(this.count - 1);
  }

  isMin() {
    return this.count <= this.min;
  }
}
