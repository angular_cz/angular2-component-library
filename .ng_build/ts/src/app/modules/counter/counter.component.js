import { Component, EventEmitter, Input, Output } from '@angular/core';
export class CounterComponent {
    constructor() {
        this.countChange = new EventEmitter();
        this.min = 0;
        this.max = 100;
    }
    /**
     * @return {?}
     */
    ngOnInit() {
        this.count = this.count || this.min;
    }
    /**
     * @return {?}
     */
    increase() {
        if (this.isMax()) {
            return;
        }
        this.countChange.emit(this.count + 1);
    }
    /**
     * @return {?}
     */
    isMax() {
        return this.count >= this.max;
    }
    /**
     * @return {?}
     */
    decrease() {
        if (this.isMin()) {
            return;
        }
        this.countChange.emit(this.count - 1);
    }
    /**
     * @return {?}
     */
    isMin() {
        return this.count <= this.min;
    }
}
CounterComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-counter',
                template: `
    <div class="input-group">
      <span class="input-group-btn">
        <button class="btn btn-info" type="button" (click)="decrease()" [disabled]="isMin()">-</button>
      </span>
      <input type="text" class="form-control" readonly [value]="count">
      <span class="input-group-btn">
        <button class="btn btn-primary" type="button" (click)="increase()" [disabled]="isMax()">+</button>
      </span>
    </div>
  `,
                styles: [`

  `]
            },] },
];
/**
 * @nocollapse
 */
CounterComponent.ctorParameters = () => [];
CounterComponent.propDecorators = {
    'count': [{ type: Input },],
    'countChange': [{ type: Output },],
    'min': [{ type: Input },],
    'max': [{ type: Input },],
};
function CounterComponent_tsickle_Closure_declarations() {
    /** @type {?} */
    CounterComponent.decorators;
    /**
     * @nocollapse
     * @type {?}
     */
    CounterComponent.ctorParameters;
    /** @type {?} */
    CounterComponent.propDecorators;
    /** @type {?} */
    CounterComponent.prototype.count;
    /** @type {?} */
    CounterComponent.prototype.countChange;
    /** @type {?} */
    CounterComponent.prototype.min;
    /** @type {?} */
    CounterComponent.prototype.max;
}
//# sourceMappingURL=counter.component.js.map