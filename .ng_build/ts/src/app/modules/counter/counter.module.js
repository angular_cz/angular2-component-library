import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CounterComponent } from './counter.component';
export class CounterModule {
}
CounterModule.decorators = [
    { type: NgModule, args: [{
                imports: [
                    CommonModule
                ],
                declarations: [CounterComponent],
                exports: [CounterComponent]
            },] },
];
/**
 * @nocollapse
 */
CounterModule.ctorParameters = () => [];
function CounterModule_tsickle_Closure_declarations() {
    /** @type {?} */
    CounterModule.decorators;
    /**
     * @nocollapse
     * @type {?}
     */
    CounterModule.ctorParameters;
}
//# sourceMappingURL=counter.module.js.map