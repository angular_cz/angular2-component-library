(function (global, factory) {
	typeof exports === 'object' && typeof module !== 'undefined' ? factory(exports, require('@angular/core'), require('@angular/common')) :
	typeof define === 'function' && define.amd ? define(['exports', '@angular/core', '@angular/common'], factory) :
	(factory((global['angular2-component-library'] = {}),global.ng.core,global.ng.common));
}(this, (function (exports,core,common) { 'use strict';

var CounterComponent = (function () {
    function CounterComponent() {
        this.countChange = new core.EventEmitter();
        this.min = 0;
        this.max = 100;
    }
    /**
     * @return {?}
     */
    CounterComponent.prototype.ngOnInit = function () {
        this.count = this.count || this.min;
    };
    /**
     * @return {?}
     */
    CounterComponent.prototype.increase = function () {
        if (this.isMax()) {
            return;
        }
        this.countChange.emit(this.count + 1);
    };
    /**
     * @return {?}
     */
    CounterComponent.prototype.isMax = function () {
        return this.count >= this.max;
    };
    /**
     * @return {?}
     */
    CounterComponent.prototype.decrease = function () {
        if (this.isMin()) {
            return;
        }
        this.countChange.emit(this.count - 1);
    };
    /**
     * @return {?}
     */
    CounterComponent.prototype.isMin = function () {
        return this.count <= this.min;
    };
    return CounterComponent;
}());
CounterComponent.decorators = [
    { type: core.Component, args: [{
                selector: 'app-counter',
                template: "\n    <div class=\"input-group\">\n      <span class=\"input-group-btn\">\n        <button class=\"btn btn-info\" type=\"button\" (click)=\"decrease()\" [disabled]=\"isMin()\">-</button>\n      </span>\n      <input type=\"text\" class=\"form-control\" readonly [value]=\"count\">\n      <span class=\"input-group-btn\">\n        <button class=\"btn btn-primary\" type=\"button\" (click)=\"increase()\" [disabled]=\"isMax()\">+</button>\n      </span>\n    </div>\n  ",
                styles: ["\n\n  "]
            },] },
];
/**
 * @nocollapse
 */
CounterComponent.ctorParameters = function () { return []; };
CounterComponent.propDecorators = {
    'count': [{ type: core.Input },],
    'countChange': [{ type: core.Output },],
    'min': [{ type: core.Input },],
    'max': [{ type: core.Input },],
};
var CounterModule = (function () {
    function CounterModule() {
    }
    return CounterModule;
}());
CounterModule.decorators = [
    { type: core.NgModule, args: [{
                imports: [
                    common.CommonModule
                ],
                declarations: [CounterComponent],
                exports: [CounterComponent]
            },] },
];
/**
 * @nocollapse
 */
CounterModule.ctorParameters = function () { return []; };

exports.CounterModule = CounterModule;
exports.ɵa = CounterComponent;

Object.defineProperty(exports, '__esModule', { value: true });

})));
//# sourceMappingURL=angular2-component-library.umd.js.map
