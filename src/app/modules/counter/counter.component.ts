import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.css']
})
export class CounterComponent implements OnInit {

  @Input() count;
  @Output() countChange = new EventEmitter<number>();
  @Input() min = 0;
  @Input() max = 100;

  constructor() {
  }

  ngOnInit() {
    this.count = this.count || this.min;
  }

  increase() {
    if (this.isMax()) {
      return;
    }

    this.countChange.emit(this.count + 1);
  }

  isMax() {
    return this.count >= this.max;
  }

  decrease() {
    if (this.isMin()) {
      return;
    }

    this.countChange.emit(this.count - 1);
  }

  isMin() {
    return this.count <= this.min;
  }
}
