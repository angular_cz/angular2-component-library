# Angular2ComponentLibrary


Připravená bootstrap aplikace, s komponentou counter se šablonou

- oživit count, plus, mínus, max, min, disabled


```
npm install ng-packagr --save-dev
```

ng-package.json
```
{
  "$schema": "./node_modules/ng-packagr/ng-package.schema.json",
  "lib": {
    "entryFile": "public_api.ts"
  }
}
```


public_api.ts

```
export * from './src/app/modules/counter/counter.module';
```

package.json
```
  "packagr": "ng-packagr -p ng-package.json"
```
